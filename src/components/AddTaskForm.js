import React from 'react';

import  './Task.css';

const AddTaskForm = (props) => (
    <div className="add-task-block">
        <input type="text"
               id={props.inputId}
               placeholder="Add new task"
               onChange={props.change}
               value={props.value}
        />
        <button id="add-btn" onClick={props.task}>Add</button>
    </div>
);

export default AddTaskForm;