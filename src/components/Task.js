import React from 'react';
import  './Task.css';

const Task = (props) => (
    <div className="task">
        <span className="task-text">
            <input type="checkbox"/>
            {props.text}

            <button
                onClick={props.remove}
                className="close-btn"
                id="close"
            >
            {props.icon}
            </button>
        </span>
    </div>
);

export default Task;