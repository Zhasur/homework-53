import React, { Component } from 'react';
import './App.css';
import AddTaskForm from './components/AddTaskForm'
import Task from './components/Task'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faIgloo, faTrash } from '@fortawesome/free-solid-svg-icons'


library.add(faIgloo);


class App extends Component {
  state = {
    tasks: [
        {task: 'Buy milk'},
        {task: 'Walk with dog'},
        {task: 'Do homework'},
    ],
      value: ''
  };

  removeTask = index => {
    const tasks = [...this.state.tasks];
    tasks.splice(index, 1);

    this.setState({tasks});
  };

    changeHandler = (event) => {
      this.setState({
          value: event.target.value
      })
    };

  addTask = () => {
    const tasks = [...this.state.tasks];
    const newTask = {task: this.state.value};

    if (newTask.task !== '') {
        tasks.push(newTask);
    } else {
      alert('No task entered!')
    }

    this.setState({tasks});
  };

  render() {

    let tasks = this.state.tasks.map((task, index) => {
      return (
          <Task
              key={index}
              text={task.task}
              remove={() => this.removeTask(index)}
              icon={<FontAwesomeIcon icon={faTrash}/>}
          />
      )
    });

    return (
      <div className="App">
          <AddTaskForm
              value={this.state.value}
              inputId="task-input"
              change={(event) => this.changeHandler(event)}
              task={this.addTask}
          />
          {tasks}
      </div>
    );
  }
}

export default App;
